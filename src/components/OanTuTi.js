import React, { Component } from "react";
import { connect } from "react-redux";
import Computer from "./Computer";
import KetQua from "./KetQua";
import "./main.css";
import Player from "./Player";

class OanTuTi extends Component {
  render() {
    return (
      <div className="container__game">
        <div className="row text-center mt-5">
          <div className="col-4">
            <Player />
          </div>
          <div className="col-4">
            <KetQua />
            <button
              onClick={() => {
                this.props.playGame();
              }}
              className="btnPlay mt-3"
            >
              Play Game
            </button>
          </div>
          <div className="col-4">
            <Computer />
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    playGame: () => {
      //Khai báo hàm lặp đi lặp lại
      let count = 0;
      let randomItem = setInterval(() => {
        dispatch({
          type: "RAN_DOM",
        });
        count += 1;
        if (count >= 10) {
          //Dừng hàm lặp
          clearInterval(randomItem);
          dispatch({
            type: "END_GAME",
          });
        }
      }, 100);
    },
  };
};

export default connect(null, mapDispatchToProps)(OanTuTi);
