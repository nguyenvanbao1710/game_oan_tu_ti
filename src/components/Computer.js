import React, { Component } from "react";
import { connect } from "react-redux";

class Computer extends Component {
  render() {
    let keyframe = `@keyframes randomItem${Date.now()}{
      0% {top:-50px;}
      25% {top:100px;}
      50% {top:-50px;}
      75% {top:100px;}
      100% {top:0;}
    }`;
    return (
      <div>
        <div className="text-center playerGame">
          <style>{keyframe}</style>
          <div className="theThink" style={{ position: "relative" }}>
            <img
              src={this.props.computer.hinhAnh}
              alt={this.props.computer.hinhAnh}
              style={{
                width: 70,
                height: 70,
                transform: "rotate(-120deg)",
                position: "absolute",
                left: "20%",
                animation: `randomItem${Date.now()} 0.5s`,
                zIndex: "10",
              }}
            />
          </div>
          <div className="speech-bubble"></div>
          <img
            src="./img/playerComputer.png"
            alt="imgPlayer"
            style={{ width: 175, height: 175 }}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    computer: state.OanTuTiReducer.computer,
  };
};

export default connect(mapStateToProps)(Computer);
