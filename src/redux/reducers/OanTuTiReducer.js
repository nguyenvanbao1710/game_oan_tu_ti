const stateDefault = {
  mangDatCuoc: [
    { ma: "keo", hinhAnh: "./img/keo.png", datCuoc: true },
    { ma: "bua", hinhAnh: "./img/bua.png", datCuoc: false },
    { ma: "bao", hinhAnh: "./img/bao.png", datCuoc: false },
  ],
  ketQua: "Bạn Thắng rồi!!",
  soBanThang: 0,
  soBanChoi: 0,
  computer: { ma: "bao", hinhAnh: "./img/bao.png" },
};

const OanTuTiReducer = (state = stateDefault, action) => {
  switch (action.type) {
    case "CHON_ITEM": {
      //reset mang dat cuoc
      let mangCuocUpdate = [...state.mangDatCuoc];
      mangCuocUpdate = mangCuocUpdate.map((item, index) => {
        if (item.ma === action.maCuoc) {
          return { ...item, datCuoc: true };
        }
        return { ...item, datCuoc: false };
      });
      state.mangDatCuoc = mangCuocUpdate;
      return { ...state };
    }
    case "RAN_DOM": {
      let soNgauNhien = Math.floor(Math.random() * 3);
      let quanCuocNgauNhien = state.mangDatCuoc[soNgauNhien];
      state.computer = quanCuocNgauNhien;
      return { ...state };
    }
    case "END_GAME": {
      state.soBanChoi += 1;
      let player = state.mangDatCuoc.find((item) => item.datCuoc === true);
      let computer = state.computer;
      switch (player.ma) {
        case "keo":
          if (computer.ma === "keo") {
            state.ketQua = "Bạn hoà nhau!!";
          } else if (computer.ma === "bua") {
            state.ketQua = "Bạn thua rồi!!";
          } else {
            state.ketQua = "Bạn thắng rồi";
            state.soBanThang += 1;
          }
          break;
        case "bua":
          if (computer.ma === "keo") {
            state.ketQua = "Bạn thắng rồi!!";
          } else if (computer.ma === "bua") {
            state.soBanThang += 1;
            state.ketQua = "Bạn hoà rồi!!";
          } else {
            state.ketQua = "Bạn thua rồi!!";
          }
          break;
        case "bao":
          if (computer.ma === "keo") {
            state.ketQua = "Bạn thua rồi!!";
          } else if (computer.ma === "bua") {
            state.ketQua = "Bạn hoà rồi!!";
          } else {
            state.ketQua = "Bạn thắng rồi!!";
            state.soBanThang += 1;
          }
          break;
        default:
          return { ...state };
      }
      return { ...state };
    }
    default:
      return { ...state };
  }
};

export default OanTuTiReducer;
